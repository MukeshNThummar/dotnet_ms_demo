﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.utility.Constant
{  
    /// <summary>
   /// Envirement wise set Domain URL in this class, as well as maintain department wise domain URL
   /// </summary>
    public static class DomainConstant
    {
        //Auth domain
        public static string Authentication = "";

        //KeyValut Domain
        public static string KeyVaultDomain = "";

        //Authorization
        public static string Authorization = "";

        //Cache
        public const string Cache = "";
    }
}
