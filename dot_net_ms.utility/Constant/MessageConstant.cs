﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.utility.Constant
{
    public class MessageConstant
    {
        public const string SuccessMessageList = "Record List Successfully"; 
        public const string SuccessMessageSave = "Record Saved Successfully"; 
        public const string SuccessMessageUpdate = "Record Updated Successfully"; 
        public const string SuccessMessageDelete = "Record Deleted Successfully"; 
        public const string NoContant = "Data Not Found"; 
        public const string InvalidRequest = "Invalid Request";

        public const string FailedMessageSave = "Record Save Failed";
    }
}
