﻿namespace dot_net_ms.utility.dbconstant
{
    public class StoredProcedureConstant
    {
        public static readonly string Get_MASTER = $"{SchemaConstant.sch_dbo}.sp_master_get";
        public static readonly string VIEWREQUEST = $"{SchemaConstant.sch_dbo}.sp_viewrequest";
       

        #region IPad APP
        /// <summary>
        /// Get Search Page Master Data
        /// </summary>
        public static readonly string APP_MASTER_LIST = $"{SchemaConstant.sch_dbo}.app_master_fill";
        /// <summary>
        /// Get Data for Stock View as Data or Count
        /// </summary>
        public static readonly string APP_STOCK_LIST = $"{SchemaConstant.sch_dbo}.APP_STOCK_FILL";
        /// <summary>
        ///  Get Data for Stock View with image path
        /// </summary>
        public static readonly string APP_MOVIE_LIST = $"{SchemaConstant.sch_dbo}.APP_MOVIE_FILL";

        #endregion

    }
}
