﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.utility.Attributes
{
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class TableTypeAttribute : Attribute
    {
        // This is a positional argument
        public TableTypeAttribute()
        {

        }

    }
}
