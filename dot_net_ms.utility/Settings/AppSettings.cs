﻿namespace dot_net_ms.utility.Settings
{
    public static class AppSettings
    {
        public static string AppName { get; set; }
        public static string SecretKey { get; set; }
        public static string AppConnectionString { get; set; }
        public static string SQLConnectionString { get; set; }
        public static string MediaHost { get; set; }
    }
}