﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace dot_net_ms.utility.Settings
{
    public static class CommonMethod
    {
        public static DataTable ToDataTable<T>(this List<T> source)
        {

            DataTable dt = new DataTable();
            var props = TypeDescriptor.GetProperties(typeof(T));
            foreach (PropertyDescriptor prop in props)
            {
                //DataColumn dc = dt.Columns.Add(prop.Name, prop.PropertyType);
                DataColumn dc = dt.Columns.Add(prop.Name, System.Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                dc.Caption = prop.DisplayName;
            }
            foreach (T item in source)
            {
                DataRow dr = dt.NewRow();
                foreach (PropertyDescriptor prop in props)
                {
                    dr[prop.Name] = prop.GetValue(item) ?? System.DBNull.Value;
                }
                dt.Rows.Add(dr);
            }
            return dt;


        }

        public static string ToCSV(this int[] value)
        {
            try
            {
                return string.Join(",", value);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
