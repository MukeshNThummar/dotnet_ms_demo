﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.repository.Extensions
{
    public static class DBContextExtensions
    {
        public static async Task<DataTable> DataTable(this DbContext context, string sqlQuery)
        {
            DataTable dt = await DataTable<object>(context, sqlQuery, null);
            return dt;
        }

        public static async Task<DataTable> DataTable<T>(this DbContext context, string sqlQuery, T objParams)
        {
            //Result data = new Result();
            DataTable dataTable = new DataTable();
            DbConnection connection = context.Database.GetDbConnection();

            try
            {
                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync();

                DbProviderFactory dbFactory = DbProviderFactories.GetFactory(connection);
                using (DbCommand cmd = dbFactory.CreateCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sqlQuery;
                    cmd.SetParams(objParams);

                    var result = await cmd.ExecuteReaderAsync();
                    dataTable.Load(result);
                }
            }
            finally
            {
                await connection.CloseAsync();
            }
            return dataTable;
            //return data;
        }

        public static async Task<DataSet> DataSet(this DbContext context, string sqlQuery)
        {
            DataSet ds = await DataSet<object>(context, sqlQuery, null);
            return ds;
        }

        public static async Task<DataSet> DataSet<T>(this DbContext context, string sqlQuery, T objParams)
        {
            //Result data = new Result();
            DataSet DS = new DataSet();
            DbConnection connection = context.Database.GetDbConnection();

            try
            {
                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync();

                DbProviderFactory dbFactory = DbProviderFactories.GetFactory(connection);
                using (DbCommand cmd = dbFactory.CreateCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sqlQuery;
                    cmd.SetParams(objParams);

                    DbDataAdapter adapter = dbFactory.CreateDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(DS);
                    DS.SetTableName();
                }
            }
            finally
            {
                await connection.CloseAsync();
            }
            return DS;
        }


        public static async Task<Boolean> ExecuteNonQueryAsync<T>(this DbContext context, string sqlQuery, T objParams)
        {

            //DataTable dataTable = new DataTable();
            DbConnection connection = context.Database.GetDbConnection();
            Boolean Issucess = false;
            try
            {
                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync();

                DbProviderFactory dbFactory = DbProviderFactories.GetFactory(connection);
                using (DbCommand cmd = dbFactory.CreateCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = sqlQuery;
                    cmd.SetParams(objParams);

                    var response = await cmd.ExecuteNonQueryAsync();
                    if (response == -1)
                    {
                        Issucess =  true;
                    }
                }
            }
            finally
            {
                await connection.CloseAsync();
                await connection.CloseAsync();
            }
            return Issucess;
            //return data;
        }
    }
}
