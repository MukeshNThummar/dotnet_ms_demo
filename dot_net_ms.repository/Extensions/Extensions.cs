﻿using dot_net_ms.utility.Attributes;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.repository.Extensions
{
    public static class Extensions
    {
        public static void SetTableName(this DataSet dataSet, string tableName = "table_name")
        {
            foreach (DataTable dataTable in dataSet?.Tables)
            {
                if (dataTable.Columns.Contains(tableName) && dataTable.Rows.Count > 0)
                {
                    string table_name_value = Convert.ToString(dataTable.Rows[0][tableName]);
                    if (!string.IsNullOrEmpty(table_name_value))
                        dataTable.TableName = table_name_value;
                    dataTable.Columns.Remove(tableName);
                }
            }

        }
        public static void SetParams<T>(this DbCommand cmd, T objParams)
        {
            if (Equals(objParams, null))
                return;


            cmd.Parameters.Clear();
            var props = TypeDescriptor.GetProperties(objParams);
            foreach (PropertyDescriptor prop in props)
            {
                if (!Equals(prop.GetValue(objParams), null))
                {
                    //cmd.Parameters.Add(new SqlParameter(prop.Name, prop.GetValue(objParams)));
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = prop.Name;
                    if (!Equals(prop.Attributes[typeof(TableTypeAttribute)], null))
                    {
                        var ListValue = prop.GetValue(objParams);
                        if (ListValue.GetType().IsSerializable)
                        {
                            string JsonData = JsonConvert.SerializeObject(ListValue);
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(JsonData);
                            dbParameter.Value = dt;
                        }
                    }
                    else
                    {
                        dbParameter.Value = prop.GetValue(objParams);
                    }
                    cmd.Parameters.Add(dbParameter);
                }
            }
        }
        public static void SetParams<T>(this SqlCommand cmd, T objParams)
        {
            if (Equals(objParams, null))
                return;

            cmd.Parameters.Clear();
            var props = TypeDescriptor.GetProperties(objParams);
            foreach (PropertyDescriptor prop in props)
            {
                if (!Equals(prop.GetValue(objParams), null))
                {
                    if (!Equals(prop.Attributes[typeof(TableTypeAttribute)], null))
                    {
                        var ListValue = prop.GetValue(objParams);
                        if (ListValue.GetType().IsSerializable)
                        {
                            string JsonData = JsonConvert.SerializeObject(ListValue);
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(JsonData);
                            cmd.Parameters.Add(new SqlParameter(prop.Name, dt));
                        }
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter(prop.Name, prop.GetValue(objParams)));
                    }
                }
            }

        }

    }
}
