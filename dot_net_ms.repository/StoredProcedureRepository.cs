﻿using dot_net_ms.contract.Repository;
using dot_net_ms.repository.DBContext;
using dot_net_ms.repository.Extensions;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.repository
{
    public class StoredProcedureRepository : IStoredProcedureRepository
    {
        private readonly API_DbContext _context;

        public StoredProcedureRepository(API_DbContext context)
        {
            _context = context;
        }

        public async Task<DataTable> GetDataTableAsync<T>(string strProcedure, T objPrams)
        {
            return await _context.DataTable(strProcedure, objPrams);
        }

        public async Task<DataTable> GetDataTableAsync(string strProcedure)
        {
            return await _context.DataTable(strProcedure);
        }

        public async Task<DataSet> GetDataSetAsync(string strProcedure)
        {
            return await _context.DataSet(strProcedure);
        }
        public async Task<DataSet> GetDataSetAsync<T>(string strProcedure, T objParams)
        {
            return await _context.DataSet(strProcedure, objParams);
        }

        public async Task<bool> ExecuteNonQueryAsync<T>(string strProcedure, T objPrams)
        {
            return await _context.ExecuteNonQueryAsync(strProcedure, objPrams);
        }
    }
}
