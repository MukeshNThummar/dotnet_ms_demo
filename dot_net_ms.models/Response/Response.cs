﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.models.Response
{
    public class Response
    {        
        public bool error_status { get; set; }

        public string message { get; set; }

        public object data { get; set; }
    }
}
