﻿using dot_net_ms.utility.Settings;

namespace dot_net_ms.models.Requests
{
    public class RequestStockView
    {
        public RequestStockView(RequestStockViewDTO viewDTO)
        {
            PKTID = viewDTO.PKTID;
            ShapeList = viewDTO.shape_codes.ToCSV();
            SizeList = viewDTO.size_codes.ToCSV();
            ClarityList = viewDTO.clarity_codes.ToCSV();
            ColorList = viewDTO.clarity_codes.ToCSV();
            party_code = viewDTO.party_code;
        }
        public string PKTID { get; set; }
        public string ShapeList { get; set; }
        public string SizeList { get; set; }
        public string ClarityList { get; set; }
        public string ColorList { get; set; }
        public int party_code { get; set; }
        public bool is_count { get; set; } = false;
    }

    public class RequestStockViewDTO
    {
        /// <summary>
        /// Packet Number to searching recoord
        /// </summary>
        /// <example>234/12</example>
        public string PKTID { get; set; }
        public int[] shape_codes { get; set; }
        public int[] size_codes { get; set; }
        public int[] clarity_codes { get; set; }
        public int[] color_codes { get; set; }
        public int party_code { get; set; }
    }
}
