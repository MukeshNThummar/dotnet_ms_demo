﻿using dot_net_ms.utility.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dot_net_ms.models.Requests
{
    public class ViewRequest
    {
        [Required]
        public int buyer_cabin_code { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string sol_buyer_party_code { get; set; }

        [Required]
        public int buyer_party_code { get; set; }

        [Required]
        public int guarantor_party_code { get; set; }

        [Required]
        public int contact_person_code { get; set; }

        [Required]
        public int pure_kam_user_code { get; set; }

        [Required]
        public int parcel_kam_user_code { get; set; }

        [Required]
        public int memo_kam_user_code { get; set; }

        [Required]
        public int sale_type_code { get; set; }

        [Required]
        public int term_code { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string client_remark { get; set; }

        [TableType]
        public List<MemoDetails> memo_detail { get; set; }

    }
    public class MemoDetails
    {
        [Required]
        public string PKTNO { get; set; }
        public int pcs { get; set; }
        [Required]
        public double carat { get; set; }
        [Required]
        public double packet_rate { get; set; }
    }
}
