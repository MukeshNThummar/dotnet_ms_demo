﻿using dot_net_ms.models.Requests;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.contract.Business
{
    public interface IStockViewBusiness
    {
        Task<DataTable> GetStockView(RequestStockView requestStockView);
        Task<DataTable> GetStockViewCount(RequestStockView requestStockView);
        Task<DataTable> GetStockMovieView(RequestStockView requestStockView);

        Task<bool> ViewRequest(ViewRequest addMemo);
    }
}
