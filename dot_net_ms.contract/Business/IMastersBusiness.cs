﻿using dot_net_ms.models.Requests;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.contract.Business
{
    public interface IMastersBusiness
    {
        Task<DataSet> GetAllMasters();
        Task<DataSet> GetMasters(MasterPayload masters);
    }
}
