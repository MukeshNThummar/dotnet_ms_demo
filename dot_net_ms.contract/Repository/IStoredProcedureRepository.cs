﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dot_net_ms.contract.Repository
{
    public interface IStoredProcedureRepository
    {
        Task<DataTable> GetDataTableAsync<T>(string strProcedure, T objPrams);
        Task<DataTable> GetDataTableAsync(string strProcedure);

        Task<DataSet> GetDataSetAsync(string strProcedure);
        Task<DataSet> GetDataSetAsync<T>(string strProcedure, T objPrams);

        Task<Boolean> ExecuteNonQueryAsync<T>(string strProcedure, T objPrams);
    }
}
