﻿using dot_net_ms.contract.Business;
using dot_net_ms.contract.Repository;
using dot_net_ms.models.Requests;
using dot_net_ms.utility.dbconstant;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.bussiness.Masters
{
    public class MastersBusiness : IMastersBusiness
    {
        private IStoredProcedureRepository _storedProcedureRepository;
        public MastersBusiness(IStoredProcedureRepository storedProcedureRepository)
        {
            _storedProcedureRepository = storedProcedureRepository;
        }
        public async Task<DataSet> GetAllMasters()
        {
            return await _storedProcedureRepository.GetDataSetAsync(StoredProcedureConstant.APP_MASTER_LIST);   
        }
        public async Task<DataSet> GetMasters(MasterPayload masters)
        {
            return await _storedProcedureRepository.GetDataSetAsync(StoredProcedureConstant.Get_MASTER, masters);
        }
    }
}
