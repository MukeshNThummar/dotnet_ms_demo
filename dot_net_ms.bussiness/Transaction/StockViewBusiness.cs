﻿using dot_net_ms.contract.Business;
using dot_net_ms.contract.Repository;
using dot_net_ms.models.Requests;
using dot_net_ms.utility.dbconstant;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.bussiness.Transaction
{
    public class StockViewBusiness : IStockViewBusiness
    {

        private IStoredProcedureRepository _storedProcedureRepository;
        public StockViewBusiness(IStoredProcedureRepository storedProcedureRepository)
        {
            _storedProcedureRepository = storedProcedureRepository;
        }

        public async Task<bool> ViewRequest(ViewRequest viewRequest)
        {
            return await _storedProcedureRepository.ExecuteNonQueryAsync(StoredProcedureConstant.VIEWREQUEST, viewRequest);
        }

        public async Task<DataTable> GetStockView(RequestStockView requestStockView)
        {
            return await _storedProcedureRepository.GetDataTableAsync(StoredProcedureConstant.APP_STOCK_LIST, requestStockView);
        }
        public async Task<DataTable> GetStockViewCount(RequestStockView requestStockView)
        {
            requestStockView.is_count = true;
            DataTable DTStock = await _storedProcedureRepository.GetDataTableAsync(StoredProcedureConstant.APP_STOCK_LIST, requestStockView);
            DataTable DTMovie = await _storedProcedureRepository.GetDataTableAsync(StoredProcedureConstant.APP_MOVIE_LIST, requestStockView);

            DataTable DTCount = new DataTable("COUNTS");
            DTCount.Columns.Add("Type", typeof(string));
            DTCount.Columns.Add("TotalRecords", typeof(string));
            if (DTStock.Rows.Count == 1 && DTStock.Columns.Contains("TotalRecords"))
            {
                DataRow DR1 = DTCount.NewRow();
                DR1["Type"] = "Stock";
                DR1["TotalRecords"] = DTStock.Rows[0]["TotalRecords"];
                DTCount.Rows.Add(DR1);
            }
            if (DTStock.Rows.Count == 1 && DTStock.Columns.Contains("TotalRecords"))
            {
                DataRow DR2 = DTCount.NewRow();
                DR2["Type"] = "Movie";
                DR2["TotalRecords"] = DTMovie.Rows[0]["TotalRecords"];
                DTCount.Rows.Add(DR2);
            }
            return DTCount;
        }

        public async Task<DataTable> GetStockMovieView(RequestStockView requestStockView)
        {
            return await _storedProcedureRepository.GetDataTableAsync(StoredProcedureConstant.APP_MOVIE_LIST, requestStockView);
        }
      
    }
}
