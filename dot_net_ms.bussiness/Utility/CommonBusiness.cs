﻿using dot_net_ms.contract.Business;
using dot_net_ms.contract.Repository;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.bussiness.Utility
{
    public class CommonBusiness : ICommonBusiness
    {
        private IStoredProcedureRepository _storedProcedureRepository;
        public CommonBusiness(IStoredProcedureRepository storedProcedureRepository)
        {
            _storedProcedureRepository = storedProcedureRepository;
        }

        public async Task<bool> ExecuteNonQueryAsync<T>(string strProcedure, T objPrams)
        {
            return await _storedProcedureRepository.ExecuteNonQueryAsync(strProcedure, objPrams);
        }

        public async Task<DataSet> GetDataSetAsync(string strProcedure)
        {
            return await _storedProcedureRepository.GetDataSetAsync(strProcedure);
        }

        public async Task<DataSet> GetDataSetAsync<T>(string strProcedure, T objPrams)
        {
            return await _storedProcedureRepository.GetDataSetAsync(strProcedure, objPrams);
        }

        public async Task<DataTable> GetDataTableAsync<T>(string strProcedure, T objPrams)
        {
            return await _storedProcedureRepository.GetDataTableAsync(strProcedure, objPrams);
        }

        public async Task<DataTable> GetDataTableAsync(string strProcedure)
        {
            return await _storedProcedureRepository.GetDataTableAsync(strProcedure);
        }
    }
}
