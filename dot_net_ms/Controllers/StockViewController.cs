﻿using dot_net_ms.Attributes;
using dot_net_ms.contract.Business;
using dot_net_ms.Exceptions.CustomExceptions;
using dot_net_ms.Exceptions.Model;
using dot_net_ms.models.Requests;
using dot_net_ms.models.Response;
using dot_net_ms.utility.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.Controllers
{
    [CustomRoute("[Controller]")]
    [ApiController]
    public class StockViewController : ControllerBase
    {
        private readonly IStockViewBusiness _stockView;
        public StockViewController(IStockViewBusiness stockView)
        {
            _stockView = stockView;
        }

       
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        [HttpPost]
        public async Task<ActionResult> GetStockViewData(RequestStockViewDTO requestStockView)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequestException(MessageConstant.InvalidRequest);
            }
            RequestStockView requestStock = new RequestStockView(requestStockView);
            DataTable result = await _stockView.GetStockView(requestStock);
            if (result.Rows.Count > 0)
            {
                return Ok(new Response
                {
                    error_status = false,
                    message = MessageConstant.SuccessMessageList,
                    data = result
                });
            }
            throw new NoContentException(MessageConstant.NoContant);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        [HttpPost("movie")]
        public async Task<ActionResult> GetStockMovieViewData(RequestStockViewDTO requestStockView)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequestException(MessageConstant.InvalidRequest);
            }
            RequestStockView requestStock = new RequestStockView(requestStockView);
            DataTable result = await _stockView.GetStockMovieView(requestStock);
            if (result.Rows.Count > 0)
            {
                return Ok(new Response
                {
                    error_status = false,
                    message = MessageConstant.SuccessMessageList,
                    data = result
                });
            }
            throw new NoContentException(MessageConstant.NoContant);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        [HttpPost("count")]
        public async Task<ActionResult> GetStockViewDataCount(RequestStockViewDTO requestStockView)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequestException(MessageConstant.InvalidRequest);
            }
            RequestStockView requestStock = new RequestStockView(requestStockView);
            DataTable result = await _stockView.GetStockViewCount(requestStock);
            if (result.Rows.Count > 0)
            {
                return Ok(new Response
                {
                    error_status = false,
                    message = MessageConstant.SuccessMessageList,
                    data = result
                });
            }
            throw new NoContentException(MessageConstant.NoContant);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        [HttpPost("Request")]
        public async Task<ActionResult> ViewRequest(ViewRequest request)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequestException(MessageConstant.InvalidRequest, request);
            }

            bool result = await _stockView.ViewRequest(request);
            if (result)
            {
                return Ok(new Response
                {
                    error_status = false,
                    message = MessageConstant.SuccessMessageSave,
                    data = null
                });
            }
            throw new System.Exception(MessageConstant.FailedMessageSave);
        }

        //[HttpGet("Search")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        //[ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(ErrorInfo))]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        //public async Task<ActionResult> SearchStockViewData()
        //{
        //    DataSet result = await _stockView.SearchStockViewData();
        //    if (result.Tables.Count > 0)
        //    {
        //        return Ok(new ResponseDataSet
        //        {
        //            error_status = false,
        //            message = MessageConstant.SuccessMessageList,
        //            data = result
        //        });
        //    }
        //    throw new NoContentException(MessageConstant.NoContant);
        //}
    }
}
