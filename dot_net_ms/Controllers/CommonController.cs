﻿using dot_net_ms.Attributes;
using dot_net_ms.contract.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dot_net_ms.Controllers
{
    [CustomRoute("[Controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly ICommonBusiness _common;
        public CommonController(ICommonBusiness common)
        {
            _common = common;
        }



    }
}
