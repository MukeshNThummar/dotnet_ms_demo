﻿using dot_net_ms.Attributes;
using dot_net_ms.contract.Business;
using dot_net_ms.Exceptions.CustomExceptions;
using dot_net_ms.Exceptions.Model;
using dot_net_ms.Middleware;
using dot_net_ms.models.Requests;
using dot_net_ms.models.Response;
using dot_net_ms.utility.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Threading.Tasks;

namespace dot_net_ms.Controllers
{
    [CustomRoute("[Controller]")]
    [ApiController]
    public class MastersController : ControllerBase
    {
        private readonly IMastersBusiness _master;
        public MastersController(IMastersBusiness master)
        {
            _master = master;
        }

        
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Response))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        [HttpGet("all")]
        public async Task<ActionResult> GetAllMasters()
        {
            DataSet result = await _master.GetAllMasters();
            if (result.Tables.Count > 0)
            {
                return Ok(new Response
                {
                    data = result,
                    error_status = false,
                    message = MessageConstant.SuccessMessageList
                });
            }
            throw new NoContentException("No Content");
        }

        
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Response))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorInfo))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErrorInfo))]
        [HttpPost]
        public async Task<ActionResult> GetMasters(MasterPayload master)
        {
            DataSet result = await _master.GetMasters(master);
            if (result.Tables.Count > 0)
            {
                return Ok(new Response
                {
                    data = result,
                    error_status = false,
                    message = MessageConstant.SuccessMessageList
                });
            }
            throw new NoContentException("No Content");
        }

    }
}
