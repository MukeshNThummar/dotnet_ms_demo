﻿using dot_net_ms.utility.Constant;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace dot_net_ms.Middleware
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Headers.ContainsKey("token"))
            {
                string token = context.Request.Headers["token"];
                if (string.IsNullOrEmpty(token))
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return;
                }

                bool res = await ValidateToken(token);
                //if (token.Equals(AppSettings.Token))
                //{
                if (res)
                {
                    await _next.Invoke(context);
                }
                else
                {
                    throw new UnauthorizedAccessException("Unauthorized Token");
                }
            }
            else
            {
                throw new UnauthorizedAccessException("Token Required!");
            }
        }

        protected async Task<bool> ValidateToken(string _token)
        {
            auth auth = new auth { token = _token };

            var client = new RestClient(ExternalURLConstant.TokenVerify);
            var request = new RestRequest(Method.POST);
            request.AddJsonBody(auth);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                JObject jObject = JsonConvert.DeserializeObject<JObject>(response.Content);

                if (jObject.ContainsKey("error_status"))
                {
                    if ((bool)jObject["error_status"] == true)
                    {
                        if (jObject.ContainsKey("message"))
                        {
                            throw new UnauthorizedAccessException(jObject["message"].ToString());
                            //Logger.Write("ValidateToken-" + jObject["message"].ToString(), Logger.LogType.Error);
                        }
                        return false;
                    }
                }
                if (!jObject.ContainsKey("data"))
                {
                    return false;
                }
                return jObject["data"].HasValues;
            }
            return false;
        }
        class auth
        {
            public string token { get; set; }
        }

    }
}
