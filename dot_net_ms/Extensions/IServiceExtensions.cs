﻿using dot_net_ms.bussiness.Masters;
using dot_net_ms.bussiness.Transaction;
using dot_net_ms.bussiness.Utility;
using dot_net_ms.contract.Business;
using dot_net_ms.contract.Repository;
using dot_net_ms.repository;
using Microsoft.Extensions.DependencyInjection;

namespace dot_net_ms.Extensions
{
    public static class IServiceExtensions
    {
        public static IServiceCollection AddScopeRepositoryServices(this IServiceCollection services)
        {
            services.AddScoped<IStoredProcedureRepository, StoredProcedureRepository>();
            return services;
        }

        public static IServiceCollection AddScopeBusinessServices(this IServiceCollection services)
        {
            services.AddScoped<IStockViewBusiness, StockViewBusiness>();
            services.AddScoped<IMastersBusiness, MastersBusiness>();
            services.AddScoped<ICommonBusiness, CommonBusiness>();
            return services;
        }
    }
}
