﻿using dot_net_ms.Middleware;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dot_net_ms.Extensions
{
    public static class AppBuilderExtension
    {
        /// <summary>
        /// Validate Token for each request of API.
        /// </summary>
        /// <param name="builder">The <see cref="IApplicationBuilder" /> to add builder to.</param>
        public static IApplicationBuilder UseCustomAuthentication(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<AuthenticationMiddleware>();
            return builder;
        }
    }
}
