﻿using dot_net_ms.utility.Settings;
using Microsoft.AspNetCore.Mvc.Routing;
using System;

namespace dot_net_ms.Attributes
{

    public sealed class CustomRouteAttribute : Attribute, IRouteTemplateProvider
    {
        private readonly string _template;
        public CustomRouteAttribute(string template)
        {
            _template = template;
        }
        public string Template => $"{AppSettings.AppName}/{_template}";

        public int? Order => 0;

        public string Name => $"{AppSettings.AppName}/{_template}";
    }
}
