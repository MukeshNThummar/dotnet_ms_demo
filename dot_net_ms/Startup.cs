using dot_net_ms.Exceptions.Extensions;
using dot_net_ms.Exceptions.Model;
using dot_net_ms.Extensions;
using dot_net_ms.repository.DBContext;
using dot_net_ms.utility.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System.Linq;

namespace dot_net_ms
{
    public class Startup
    {
        private readonly ILogger _logger;
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            using var logFactory = LoggerFactory.Create(logger =>
            {
                logger.SetMinimumLevel(LogLevel.Debug);
                logger.AddConsole();
                logger.AddEventSourceLogger();
            });
            _logger = logFactory.CreateLogger<Startup>();
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //AddControllers Required/ AddJsonOptions used to change Json Response properties casing pascal/camel case set to null gives as its name in response model
            services.AddControllers()
                //.AddNewtonsoftJson()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new LowerCasePropertyNamesContractResolver();


                    //// Configure a custom converter
                    //options.SerializerOptions.Converters.Add(new MyCustomJsonConverter());
                })
                //.AddJsonOptions(opt => { opt.JsonSerializerOptions.PropertyNamingPolicy = null; })
                .ConfigureApiBehaviorOptions(opt =>
                {
                    opt.InvalidModelStateResponseFactory = actionContext =>
                    {
                        ModelError[] err = actionContext.ModelState.SelectMany(x => x.Value.Errors).ToArray();
                        ErrorInfo info = new ErrorInfo
                        {
                            message = "Bad Request",
                            request_path = $"{actionContext.HttpContext.Request.Method} {actionContext.HttpContext.Request.Scheme}://{actionContext.HttpContext.Request.Host.Value}{actionContext.HttpContext.Request.Path}",
                            error = err
                        };
                        _logger.LogError(info.ToJson());
                        return new BadRequestObjectResult(info);
                    };
                });


            //Set CORS Policy Allow from any Source otherwise it will cors error in browser if this api integrate in ui
            services.AddCors(cors => cors.AddPolicy("CORSPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

           
            //Get SQL passsword in keyvault API
            //string SecretValue = KeyVault.GetSQLPassword(AppSettings.SecretKey);
            string SecretValue = AppSettings.SecretKey;

            //SQL Password merge in Connection String
            AppSettings.SQLConnectionString = $"{ AppSettings.AppConnectionString};password={SecretValue}";

            //services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // Register SQL database configuration context as services.  
            services.AddDbContext<API_DbContext>(options =>
            {
                options.UseSqlServer(AppSettings.SQLConnectionString);
            });

            // Register All Repository Services
            services.AddScopeRepositoryServices();

            // Register All Business Services
            services.AddScopeBusinessServices();

            //services.AddScoped<ActionResultFilter>();

            services.AddSwaggerGen(c =>
            {
                //Add Running Versions
                c.SwaggerDoc("V1", new OpenApiInfo { Title = "DotNet", Version = "V1" });
                //c.SchemaFilter<ExampleSchemaFilter>();
                // Configure Security (Header Parameters) Settings to accessing api
                c.AddSecurityDefinition("Token", new OpenApiSecurityScheme()
                {
                    Name = "Token",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "http",
                    BearerFormat = "GUID",
                    In = ParameterLocation.Header,
                    Description = "Enter valid token in the text input below. \r\n\r\nExample: \"c3b8e5de-8149-4c11-afee-b67ae6fc62ff\""
                });

                // Configure Input Types for Security Settings
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Token"
                        }
                    },
                    new string[] {}
                  }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            logger.LogInformation("App Started");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(c =>
                {
                    c.RouteTemplate = AppSettings.AppName + "/swagger/{documentName}/swagger.json";
                });
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/{AppSettings.AppName}/swagger/V1/swagger.json", "DotNet V1");
                    c.RoutePrefix = $"{AppSettings.AppName}/swagger";
                });
            }
            app.UseCors("CORSPolicy");
           
            app.UseCustomExceptionHandler().UseCustomAuthentication();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
