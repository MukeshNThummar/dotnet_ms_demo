using dot_net_ms.utility.Constant;
using dot_net_ms.utility.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace dot_net_ms
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((context, config) =>
            {
                var builtConfig = config.Build();

                AppSettings.AppName = Convert.ToString(builtConfig["AppSettings:AppName"]);
                AppSettings.SecretKey = Convert.ToString(builtConfig["AppSettings:SecretKey"]);
                AppSettings.AppConnectionString = Convert.ToString(builtConfig["AppSettings:AppConnectionString"]);
                AppSettings.MediaHost = Convert.ToString(builtConfig["AppSettings:MediaHost"]);
                DomainConstant.Authentication = Convert.ToString(builtConfig["AppSettings:Authentication"]);
            })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });

    }
}
