﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dot_net_ms.Filters
{
    public class ActionResultFilter : IAsyncResultFilter, IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            await next();
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            //if (context.Result is ObjectResult objectResult)
            //{
            //    objectResult.Value = new ApiResult { Data = objectResult.Value };
            //}
            await next();
        }

        public class ApiResult
        {
            //public int StatusCode { get; set; }
            //public string Message { get; set; }
            public object Data { get; set; }
        }
    }
}
