﻿using System;
using System.Globalization;

namespace dot_net_ms.Exceptions.CustomExceptions
{
    public class NoContentException : Exception
    {
        public NoContentException() : base() { }

        public NoContentException(string message) : base(message) { }

        public NoContentException(string message, params object[] args) : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}
