﻿using System.Text.Json;

namespace dot_net_ms.Exceptions.Model
{
    public class ErrorInfo
    {
        public string message { get; set; }
        public object error { get; set; }
        public string request_path { get; set; }

        public string ToJson()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
