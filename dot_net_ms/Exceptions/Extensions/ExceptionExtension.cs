﻿using dot_net_ms.Exceptions.Middleware;
using Microsoft.AspNetCore.Builder;

namespace dot_net_ms.Exceptions.Extensions
{
    public static class ExceptionExtension
    {
        /// <summary>
        /// Handle Exceptions globally using ExceptionHandler
        /// </summary>
        /// <param name="builder">The <see cref="IApplicationBuilder" /> to add builder to.</param>
        public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<ExceptionMiddleware>();
            return builder;
        }

    }
}
