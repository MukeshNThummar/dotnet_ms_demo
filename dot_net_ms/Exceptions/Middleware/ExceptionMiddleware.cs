﻿using dot_net_ms.Exceptions.CustomExceptions;
using dot_net_ms.Exceptions.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace dot_net_ms.Exceptions.Middleware
{
    public class ExceptionMiddleware
    {
        RequestDelegate _next;
        private readonly ILogger _logger;
        public ExceptionMiddleware(ILogger<ExceptionMiddleware> logger, RequestDelegate next)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                HttpResponse response = context.Response;
                response.ContentType = "application/json";

                switch (error)
                {
                    case BadRequestException:
                        // custom application error
                        response.StatusCode = StatusCodes.Status400BadRequest;
                        break;
                    case NoContentException:
                        response.StatusCode = StatusCodes.Status204NoContent;
                        break;
                    case UnauthorizedAccessException:
                        response.StatusCode = StatusCodes.Status401Unauthorized;
                        break;
                    default:
                        // unhandled error
                        response.StatusCode = StatusCodes.Status500InternalServerError;
                        break;
                }

                ErrorInfo info = new ErrorInfo
                {
                    message = error.Message,
                    request_path = $"{context.Request.Method} {context.Request.Scheme}://{context.Request.Host.Value}{context.Request.Path}"
                    //,error = error.StackTrace
                };
                _logger.LogError(error.Message, error);
                await response.WriteAsync(info.ToJson());
            }
        }
    }
}